# Tablet Stand
A laser cut stand for Tablets.

![Image of stand](https://github.com/gratefulfrog/TabletStand/blob/master/images/IMG_20170125_174103.jpg)
![Image of stand](https://github.com/gratefulfrog/TabletStand/blob/master/images/IMG_20170125_174112.jpg)
![Image of stand](https://github.com/gratefulfrog/TabletStand/blob/master/images/img_20170123_185951_32106760670_o.jpg)
